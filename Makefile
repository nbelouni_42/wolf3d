# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/11/30 11:19:25 by nbelouni          #+#    #+#              #
#    Updated: 2015/02/18 16:02:38 by nbelouni         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME= wolf3d 
CC= gcc
CFLAGS= -Wall -Werror -Wextra
XFLAGS= -L /usr/X11/lib -lmlx -lxext -lX11
HFLAGS= -I includes
LFLAGS= -I libft/includes/
SRC= src/draw_map.c src/ft_free_tab.c src/ft_get_itab.c src/get_next_line.c\
	 src/init_env.c src/keyboard.c src/main.c src/print_image.c src/raycast.c\
	 src/keyboard2.c
OBJ= draw_map.o ft_free_tab.o ft_get_itab.o get_next_line.o init_env.o\
	 keyboard.o main.o print_image.o  raycast.o keyboard2.o

all: $(NAME)

$(NAME): $(OBJ) 
	$(CC) $(XFLAGS) -o $(NAME) $(OBJ) -L libft -lft

$(OBJ): $(SRC)
	make -C libft
	$(CC) $(CFLAGS) $(XFLAGS) $(LFLAGS) $(HFLAGS) -c $(SRC)

clean:
	rm -f $(OBJ)
	make -C libft clean

fclean: clean
	rm -f $(NAME)
	make -C libft fclean

re: clean all
