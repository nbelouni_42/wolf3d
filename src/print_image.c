/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_image.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/15 04:30:59 by nbelouni          #+#    #+#             */
/*   Updated: 2015/01/08 11:23:39 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

int			put_pixel_on_image(void *img, int x, int y, int color)
{
	char	*data;
	int		i;
	int		bpp;
	int		sl;
	int		endian;

	data = mlx_get_data_addr(img, &bpp, &sl, &endian);
	bpp /= 8;
	i = x * bpp + y * sl;
	if (x >= HEIGHT || y >= WIDTH)
		return (0);
	data[i] = color;
	data[i + 1] = color >> 8;
	data[i + 2] = color >> 16;
	return (0);
}

int			choose_color(int side, t_coord step)
{
	int		color;

	if (side > 0 && step.i > 0)
		color = 0xf9f9f9;
	else if (side > 0 && step.i < 0)
		color = 0x9999ff;
	else if (side == 0 && step.j > 0)
		color = 0x99ff9f;
	else if (side == 0 && step.j < 0)
		color = 0xf9f999;
	return (color);
}

void		print_col(t_coord draw, int i, int color, t_env *e)
{
	int		y;

	y = 0;
	while (y < WIDTH - 1)
	{
		if (y > draw.i && y < draw.j)
		{
			if (y - 2 < draw.i && y > draw.i)
				put_pixel_on_image(e->img, i, y, 0x000000);
			else if (y + 2 > draw.j && y < draw.j)
				put_pixel_on_image(e->img, i, y, 0x000000);
			else
				put_pixel_on_image(e->img, i, y, color);
		}
		else if (y > draw.i)
			put_pixel_on_image(e->img, i, y, 0x999999);
		else
			put_pixel_on_image(e->img, i, y, 0x666666);
		y++;
	}
}
