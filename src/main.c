/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/29 06:14:56 by nbelouni          #+#    #+#             */
/*   Updated: 2015/03/02 02:50:03 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

int			expose_hook(t_env *env)
{
	ray_start(env);
	mlx_put_image_to_window(env->mlx, env->win, env->img, 0, 0);
	return (0);
}

int			key_hook(int keycode, t_env *env)
{
	if (keycode == 65307)
		exit (0);
	if (keycode == 65505)
	{
		if (env->run == 1)
			env->run = 3;
		else if (env->run == 3)
			env->run = 1;
		else if (env->run == 0.5)
			env->run = 1.5;
		else
			env->run = 0.5;
	}
	if (keycode == 32)
	{
		if (env->jump == 0)
			jump(env);
		else
			squat_jump(env);
	}
	if (keycode == 65507)
		squatting(env);
	return (0);
}

int			main(void)
{
	t_env *env;

	if (!(env = init_env()))
		return (0);
	ray_start(env);
	mlx_put_image_to_window(env->mlx, env->win, env->img, 0, 0);
	mlx_key_hook(env->win, key_hook, env);
	mlx_hook(env->win, 2, 1L << 0, k, env);
	mlx_expose_hook(env->win, expose_hook, env);
	mlx_loop(env->mlx);
	return (0);
}
