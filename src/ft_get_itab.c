/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_itab.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/28 15:28:33 by nbelouni          #+#    #+#             */
/*   Updated: 2015/03/02 03:00:28 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

static int		*ft_itab(int *tab, char **s, int size)
{
	int		i;
	int		j;

	i = 0;
	j = 1;
	while (i < size)
	{
		tab[j] = ft_atoi(s[i]);
		i++;
		j++;
	}
	return (tab);
}

static int		*ft_cptab(int *tab1, int *tab2)
{
	int		j;

	j = 0;
	while (j < tab2[0])
	{
		tab1[j] = tab2[j];
		j++;
	}
	return (tab1);
}

static int		**ft_bigtab(int **tabb, char *s, int i_max)
{
	int		i;
	int		len;
	int		**new;
	char	**tabl;

	len = 0;
	tabl = ft_strsplit(s, ' ');
	while (tabl[len])
		len++;
	if (!(new = (int **)malloc(sizeof(int *) * (i_max))))
		return (NULL);
	i = -1;
	while (++i < i_max - 1)
	{
		if (!(new[i] = (int *)malloc(sizeof(int) * tabb[i][0])))
			return (NULL);
		new[i] = ft_cptab(new[i], (tabb[i]));
	}
	if (!(new[i] = (int *)malloc(sizeof(int) * len + 1)))
		return (NULL);
	new[i][0] = len + 1;
	new[i] = ft_itab(new[i], tabl, len);
	tabl = ft_free_ctab(tabl, len);
	tabb = ft_free_itab(tabb, i_max - 1);
	return (new);
}

int				**ft_get_itab(char *argv)
{
	int		fd;
	int		**tab;
	int		i;
	char	*s;

	i = 2;
	s = NULL;
	fd = open(argv, O_RDONLY);
	if (!(tab = (int **)malloc(sizeof(int *))))
		return (NULL);
	if (!(*tab = (int *)malloc(sizeof(int) * 2)))
		return (NULL);
	tab[0][0] = 2;
	while (get_next_line(fd, &s) > 0 && (tab[0][1] = i++))
	{
		if (!(tab = ft_bigtab(tab, s, i - 1)))
			return (NULL);
		ft_strdel(&s);
	}
	if (get_next_line(fd, &s) < 0)
		return (NULL);
	tab[0][1] = i - 1;
	if (close(fd) < 0)
		return (NULL);
	return (tab);
}
