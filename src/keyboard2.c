/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   keyboard2.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/18 13:57:31 by nbelouni          #+#    #+#             */
/*   Updated: 2015/03/02 03:00:54 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

int			k1(int keycode, t_env *e)
{
	t_point	old;
	double	m;

	m = 0.1 * e->run;
	old.x = e->init.dir.x;
	old.y = e->init.delta.x;
	if (keycode == 65363)
	{
		e->init.dir.x = e->init.dir.x * cos(-m) - e->init.dir.y * sin(-m);
		e->init.dir.y = old.x * sin(-m) + e->init.dir.y * cos(-m);
		e->init.delta.x = e->init.delta.x * cos(-m) - e->init.delta.y * sin(-m);
		e->init.delta.y = old.y * sin(-m) + e->init.delta.y * cos(-m);
		ray_start(e);
		mlx_put_image_to_window(e->mlx, e->win, e->img, 0, 0);
	}
	if (keycode == 65361)
	{
		e->init.dir.x = e->init.dir.x * cos(m) - e->init.dir.y * sin(m);
		e->init.dir.y = old.x * sin(m) + e->init.dir.y * cos(m);
		e->init.delta.x = e->init.delta.x * cos (m) - e->init.delta.y * sin(m);
		e->init.delta.y = old.y * sin(m) + e->init.delta.y * cos(m);
		ray_start(e);
		mlx_put_image_to_window(e->mlx, e->win, e->img, 0, 0);
	}
	return (0);
}

int			k2(int keycode, t_env *e)
{
	t_point	rot;

	rot.x = e->init.dir.x * 0.32 * e->run;
	rot.y = e->init.dir.y * 0.32 * e->run;
	if (keycode == 65362)
	{
		if (e->world[(int)(e->init.pos.x + rot.x)][(int)e->init.pos.y] <= 0)
			e->init.pos.x += rot.x;
		if (e->world[(int)e->init.pos.x][(int)(e->init.pos.y + rot.y)] <= 0)
			e->init.pos.y += rot.y;
		ray_start(e);
		mlx_put_image_to_window(e->mlx, e->win, e->img, 0, 0);
	}
	if (keycode == 65364)
	{
		if (e->world[(int)(e->init.pos.x - rot.x)][(int)e->init.pos.y] <= 0)
			e->init.pos.x -= rot.x;
		if (e->world[(int)e->init.pos.x][(int)(e->init.pos.y - rot.y)] <= 0)
			e->init.pos.y -= rot.y;
		ray_start(e);
		mlx_put_image_to_window(e->mlx, e->win, e->img, 0, 0);
	}
	return (0);
}

int			k(int keycode, t_env *env)
{
	k1(keycode, env);
	k2(keycode, env);
	return (0);
}
