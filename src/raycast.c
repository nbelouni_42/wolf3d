/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   raycast.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/31 20:29:28 by nbelouni          #+#    #+#             */
/*   Updated: 2015/03/02 02:57:49 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

static int		dda(t_env *e, t_base *dist, t_coord step)
{
	int			wall;
	int			side;

	wall = 0;
	while (wall == 0)
	{
		if (dist->pos.x < dist->pos.y)
		{
			dist->pos.x += dist->delta.x;
			e->map.i += step.i;
			side = 0;
		}
		else
		{
			dist->pos.y += dist->delta.y;
			e->map.j += step.j;
			side = 1;
		}
		if (e->world[e->map.i][e->map.j] > 0)
			wall = 1;
	}
	return (side);
}

static void		draw_image(t_env *e, int i, t_base dist, t_coord step)
{
	t_coord		draw;
	double		mystic;
	t_coord		d;
	int			line_height;
	int			side;

	d.i = (1 - step.i) / 2;
	d.j = (1 - step.j) / 2;
	if ((side = dda(e, &dist, step) == 0))
		mystic = fabs((e->map.i - e->init.pos.x + d.i) / dist.dir.x);
	else
		mystic = fabs((e->map.j - e->init.pos.y + d.j) / dist.dir.y);
	line_height = abs((int)(WIDTH / mystic));
	draw.i = -line_height / 2 + WIDTH / (2 + e->sight.y) + e->jump;
	if (draw.i < 0)
		draw.i = 0;
	draw.j = line_height / 2 + WIDTH / 2 + e->jump;
	if (draw.j >= WIDTH)
		draw.j = WIDTH - 1;
	print_col(draw, i, choose_color(side, step), e);
}

static int		find_side(t_base *dist, t_env *e, t_coord *step)
{
	e->map.i = (int)e->init.pos.x;
	e->map.j = (int)e->init.pos.y;
	if (dist->dir.x < 0)
	{
		step->i = -1;
		dist->pos.x = (e->init.pos.x - e->map.i) * dist->delta.x;
	}
	else
	{
		step->i = 1;
		dist->pos.x = (e->map.i + 1.0 - e->init.pos.x) * dist->delta.x;
	}
	if (dist->dir.y < 0)
	{
		step->j = -1;
		dist->pos.y = (e->init.pos.y - e->map.j) * dist->delta.y;
	}
	else
	{
		step->j = 1;
		dist->pos.y = (e->map.j + 1.0 - e->init.pos.y) * dist->delta.y;
	}
	return (0);
}

int				ray_start(t_env *e)
{
	t_base		p;
	t_coord		step;
	double		sight;
	int			i;

	i = 0;
	while (i < HEIGHT)
	{
		sight = 2 * i / (double)HEIGHT - 1;
		p.dir.x = e->init.dir.x + e->init.delta.x * sight;
		p.dir.y = e->init.dir.y + e->init.delta.y * sight;
		i++;
		p.delta.x = sqrt(1 + (p.dir.y * p.dir.y) / (p.dir.x * p.dir.x));
		p.delta.y = sqrt(1 + (p.dir.x * p.dir.x) / (p.dir.y * p.dir.y));
		find_side(&p, e, &step);
		draw_image(e, i, p, step);
	}
	init_block(e);
	draw_map(e);
	return (0);
}
