/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_env.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/07 08:03:07 by nbelouni          #+#    #+#             */
/*   Updated: 2015/01/19 22:58:38 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

t_env			*init_env(void)
{
	t_env		*env;

	if (!(env = (t_env *)malloc(sizeof(t_env))))
		return (NULL);
	env->run = 1;
	env->sight.y = 0;
	env->jump = 0;
	env->init.pos.x = 4;
	env->init.pos.y = 4;
	env->init.dir.x = -1;
	env->init.dir.y = 0;
	env->init.delta.x = 0;
	env->init.delta.y = 0.66;
	if (!(env->mlx = mlx_init()))
		return (NULL);
	if (!(env->win = mlx_new_window(env->mlx, HEIGHT, WIDTH, "wolf3d")))
		return (NULL);
	if (!(env->img = mlx_new_image(env->mlx, HEIGHT, WIDTH)))
		return (NULL);
	if (!(env->world = ft_get_itab("maps/map")))
		return (NULL);
	return (env);
}

void			init_block(t_env *e)
{
	if (e->world[(int)e->init.pos.x - 1][(int)e->init.pos.y] == 0)
	{
		e->world[(int)e->init.pos.x - 1][(int)e->init.pos.y] -= 1;
		if (e->world[(int)e->init.pos.x - 2][(int)e->init.pos.y] == 0)
			e->world[(int)e->init.pos.x - 2][(int)e->init.pos.y] -= 1;
	}
	if (e->world[(int)e->init.pos.x + 1][(int)e->init.pos.y] == 0)
	{
		e->world[(int)e->init.pos.x + 1][(int)e->init.pos.y] -= 1;
		if (e->world[(int)e->init.pos.x + 2][(int)e->init.pos.y] == 0)
			e->world[(int)e->init.pos.x + 2][(int)e->init.pos.y] -= 1;
	}
	if (e->world[(int)e->init.pos.x][(int)e->init.pos.y - 1] == 0)
	{
		e->world[(int)e->init.pos.x][(int)e->init.pos.y - 1] -= 1;
		if (e->world[(int)e->init.pos.x][(int)e->init.pos.y - 2] == 0)
			e->world[(int)e->init.pos.x][(int)e->init.pos.y - 2] -= 1;
	}
	if (e->world[(int)e->init.pos.x][(int)e->init.pos.y + 1] == 0)
	{
		e->world[(int)e->init.pos.x][(int)e->init.pos.y + 1] -= 1;
		if (e->world[(int)e->init.pos.x][(int)e->init.pos.y + 2] == 0)
			e->world[(int)e->init.pos.x][(int)e->init.pos.y + 2] -= 1;
	}
}
