/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_map.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 1015/01/07 00:38:26 by nbelouni          #+#    #+#             */
/*   Updated: 2015/03/02 02:53:38 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

static void		draw_point(void *img, int h, int w)
{
	put_pixel_on_image(img, 50 + w + 6, 50 + h + 5, 0xff0000);
	put_pixel_on_image(img, 50 + w + 5, 50 + h + 6, 0xff0000);
	put_pixel_on_image(img, 50 + w + 5, 50 + h + 5, 0xff0000);
	put_pixel_on_image(img, 50 + w + 6, 50 + h + 6, 0xff0000);
}

static void		wall(void *img, int w, int h, int b)
{
	int		i;

	i = 0;
	while (i <= 10)
	{
		if (b)
			put_pixel_on_image(img, i + w, h, 0x333333);
		else
			put_pixel_on_image(img, w, i + h, 0x333333);
		i++;
	}
}

static void		draw_wall(void *img, int h, int w, int b)
{
	if (!b)
		wall(img, 50 + w, 50 + h, 1);
	else if (b == 1)
		wall(img, 50 + w, 60 + h, 1);
	else if (b == 2)
		wall(img, 50 + w, 50 + h, 0);
	else if (b == 3)
		wall(img, 60 + w, 50 + h, 0);
}

static void		choose(t_env *e, t_coord p, int h, int w)
{
	if (e->world[p.i - 1][p.j] > 0)
		draw_wall(e->img, h, w, 0);
	if (e->world[p.i + 1][p.j] > 0)
		draw_wall(e->img, h, w, 1);
	if (e->world[p.i][p.j - 1] > 0)
		draw_wall(e->img, h, w, 2);
	if (e->world[p.i][p.j + 1] > 0)
		draw_wall(e->img, h, w, 3);
}

void			draw_map(t_env *e)
{
	t_coord	p;
	int		h;
	int		w;

	p.i = 2;
	h = 0;
	while (p.i < e->world[0][1])
	{
		p.j = 2;
		w = 0;
		while (p.j < e->world[p.i][0])
		{
			if (e->world[p.i][p.j] == -1)
			{
				choose(e, p, h, w);
				if ((int)e->init.pos.x == p.i && (int)e->init.pos.y == p.j)
					draw_point(e->img, h, w);
			}
			p.j++;
			w += 10;
		}
		p.i++;
		h += 10;
	}
}
