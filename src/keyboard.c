/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   keyboard.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/07 07:17:34 by nbelouni          #+#    #+#             */
/*   Updated: 2015/01/08 11:23:11 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void		jump(t_env *env)
{
	while (env->jump < 70)
	{
		ray_start(env);
		mlx_put_image_to_window(env->mlx, env->win, env->img, 0, 0);
		env->jump += 10;
		usleep(35000);
	}
	while (env->jump > 0)
	{
		ray_start(env);
		mlx_put_image_to_window(env->mlx, env->win, env->img, 0, 0);
		env->jump -= 10;
		usleep(35000);
	}
}

void		squat_jump(t_env *env)
{
	while (env->jump < -30)
	{
		ray_start(env);
		mlx_put_image_to_window(env->mlx, env->win, env->img, 0, 0);
		env->jump += 10;
		usleep(20000);
	}
	while (env->jump > -80)
	{
		ray_start(env);
		mlx_put_image_to_window(env->mlx, env->win, env->img, 0, 0);
		env->jump -= 10;
		usleep(20000);
	}
}

void		squatting(t_env *env)
{
	if (env->jump == 0 && env->sight.y == 0)
	{
		env->jump = -80;
		env->sight.y = 0.1;
		env->run = 0.5;
		ray_start(env);
		mlx_put_image_to_window(env->mlx, env->win, env->img, 0, 0);
	}
	else
	{
		env->jump = 0;
		env->sight.y = 0;
		env->run = 1;
		ray_start(env);
		mlx_put_image_to_window(env->mlx, env->win, env->img, 0, 0);
	}
}
