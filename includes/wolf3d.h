/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf3d.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/29 06:15:24 by nbelouni          #+#    #+#             */
/*   Updated: 2015/02/18 16:03:48 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WOLF3D_H
# define WOLF3D_H
# include <mlx.h>
# include <math.h>
# include <stdlib.h>
# include <libft.h>
# include <fcntl.h>
# include <sys/types.h>
# define BUFF_SIZE 1
# define HEIGHT 800
# define WIDTH 600

typedef struct	s_coord
{
	int			i;
	int			j;
}				t_coord;

typedef struct	s_point
{
	double		x;
	double		y;
}				t_point;

typedef struct	s_base
{
	t_point		pos;
	t_point		dir;
	t_point		delta;
}				t_base;

typedef struct	s_env
{
	void		*mlx;
	void		*win;
	void		*img;
	t_base		init;
	t_coord		map;
	t_point		sight;
	int			**world;
	double		run;
	int			jump;
}				t_env;

int				put_pixel_on_image(void *img, int x, int y, int color);
int				**ft_get_itab(char *argv);
int				get_next_line(int fd, char **s);
char			**ft_free_ctab(char **tab, size_t size);
int				**ft_free_itab(int **tab, size_t size);
int				ray_start(t_env *e);
void			draw_map(t_env *e);
int				k1(int keycode, t_env *e);
int				k2(int keycode, t_env *e);
int				k(int keycode, t_env *e);
void			jump(t_env *env);
void			squatting(t_env *env);
void			squat_jump(t_env *env);
t_env			*init_env(void);
void			init_block(t_env *e);
void			print_col(t_coord draw, int i, int color, t_env *e);
int				choose_color(int side, t_coord step);

#endif
